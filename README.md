# Parasoft Jtest Integration for GitLab

This project provides example pipelines that demonstrate how to integrate Parasoft Jtest with GitLab. The integration enables you to run code analysis, collect code coverage data and test results with Parasoft Jtest and review results directly in GitLab.

Parasoft Jtest is a testing tool that automates software quality practices for Java applications. It uses a comprehensive set of analysis techniques, including pattern-based static analysis, dataflow analysis, metrics, code coverage, and unit testing to help you verify code quality and ensure compliance with industry standards, such as CWE, OWASP, and CERT.
- Request [a free trial](https://www.parasoft.com/products/parasoft-jtest/jtest-request-a-demo/) to receive access to Parasoft Jtest's features and capabilities.
- See the [user guide](https://docs.parasoft.com/display/JTEST20231) for information about Parasoft Jtest's capabilities and usage.

Please visit the [official Parasoft website](http://www.parasoft.com) for more information about Parasoft Jtest and other Parasoft products.

- [Static Analysis](#static-analysis)
  - [Quick start](#quick-start-SA)
  - [Example Pipelines](#example-pipelines-SA)
  - [Reviewing Analysis Results](#reviewing-analysis-results)
- [Unit Tests](#unit-tests)
  - [Quick start](#quick-start-UT)
  - [Example Pipelines](#example-pipelines-UT)
  - [Reviewing Test Results](#reviewing-test-results)
- [Code Coverage](#code-coverage)
  - [Quick start](#quick-start-CC)
  - [Example Pipelines](#example-pipelines-CC)
  - [Reviewing Coverage Data](#reviewing-coverage-data)

## Static Analysis

### <a name="quick-start-SA"></a> Quick start

To analyze your code with Parasoft Jtest and review analysis results in GitLab, you need to customize your pipeline to include a job that will:
* build the tested project with Gradle or Maven.
* run Jtest.
* upload the analysis report in the SAST format.
* upload the Jtest analysis reports in other formats (XML, HTML, etc.).

#### Prerequisites

* This extension requires Parasoft Jtest 2021.2 (or newer) with a valid Parasoft license. Starting with GitLab v.16.0.0 and later only SAST report v.15 is accepted. SAST report v.15 is supported in Jtest v.2023.1.1 and later.
* We recommend that you execute the pipeline on a GitLab runner with Parasoft Jtest installed and configured on the runner.

### <a name="example-pipelines-SA"></a> Example Pipelines
The following examples show simple pipelines for Gradle and Maven projects. The examples assume that Jtest is run on a GitLab runner and the path to the `jtestcli` executable is available on `PATH`.

##### Run Jtest with Gradle project

See also the example [.gitlab-ci.yml](https://gitlab.com/parasoft/jtest-gitlab/-/blob/master/pipelines/jtest-gradle/.gitlab-ci.yml) file.


```yaml
# This is a basic pipeline to help you get started with Jtest integration to analyze a Gradle project.

stages:        
  - test

# Builds the project with Gradle to run code analysis with Jtest.
StaticAnalysis:
  stage: test
  script:
    # When running on Windows with PowerShell 5.1, be sure to enforce the default file encoding:
      # - $PSDefaultParameterValues['Out-File:Encoding'] = 'default'

    # Configures advanced reporting options and SCM integration.
    - echo "report.format=xml,html,sast-gitlab" > report.properties
    - echo "report.scontrol=min" >> report.properties
    - echo "scontrol.rep.type=git" >> report.properties
    - echo "scontrol.rep.git.url=$CI_PROJECT_URL" >> report.properties
    - echo "scontrol.branch=$CI_COMMIT_BRANCH" >> report.properties
    # When running on Windows, be sure to escape backslashes:
      # - echo "scontrol.rep.git.workspace=$CI_PROJECT_DIR".Replace("\", "\\") >> report.properties
    - echo "scontrol.rep.git.workspace=$CI_PROJECT_DIR" >> report.properties

    # Launches Jtest.
    - echo "Running Jtest..."
    - gradle build -I path/to/jtest/integration/gradle/init.gradle jtest "-Djtest.config=builtin://Recommended Rules" "-Djtest.settings=report.properties" "-Djtest.report=reports"

  artifacts:
    # Uploads analysis results in the GitLab SAST format, so that they are displayed in GitLab.
    reports:
      sast: reports/report.sast
    # Uploads all report files (.xml, .html, .sast) as build artifacts.
    paths:
      - reports/*
```

##### Run Jtest with Maven project

See also the example [.gitlab-ci.yml](https://gitlab.com/parasoft/jtest-gitlab/-/blob/master/pipelines/jtest-maven/.gitlab-ci.yml) file.


```yaml
# This is a basic pipeline to help you get started with Jtest integration to analyze a Maven project.

stages:
  - test

# Builds the project with Maven to run code analysis with Jtest.
StaticAnalysis:
  stage: test
  script:
    # When running on Windows with PowerShell 5.1, be sure to enforce the default file encoding:
      # - $PSDefaultParameterValues['Out-File:Encoding'] = 'default'

    # Configures advanced reporting options and SCM integration.
    - echo "report.format=xml,html,sast-gitlab" > report.properties
    - echo "report.scontrol=min" >> report.properties
    - echo "scontrol.rep.type=git" >> report.properties
    - echo "scontrol.rep.git.url=$CI_PROJECT_URL" >> report.properties
    - echo "scontrol.branch=$CI_COMMIT_BRANCH" >> report.properties
    # When running on Windows, be sure to escape backslashes:
      # - echo "scontrol.rep.git.workspace=$CI_PROJECT_DIR".Replace("\", "\\") >> report.properties
    - echo "scontrol.rep.git.workspace=$CI_PROJECT_DIR" >> report.properties

    # Launches Jtest.
    - echo "Running Jtest..."
    - mvn install jtest:jtest "-Djtest.config=builtin://Recommended Rules" "-Djtest.settings=report.properties" "-Djtest.report=reports"

  artifacts:
    # Uploads analysis results in the GitLab SAST format, so that they are displayed in GitLab.
    reports:
      sast: reports/report.sast
    # Uploads all report files (.xml, .html, .sast) as build artifacts.
    paths:
      - reports/*
```

### Reviewing Analysis Results
When the pipeline completes, you can review the violations reported by Jtest as code vulnerabilities:
* in the *Security* tab of the GitLab pipeline.
* on GitLab's Vulnerability Report.

You can click each violation reported by Jtest to review the details and navigate to the code that triggered the violation.

### Baselining Static Analysis Results in Merge Requests
In GitLab, when a merge request is created, static analysis results generated for the branch to be merged are compared with the results generated for the integration branch. As a result, only new violations are presented in the merge request view, allowing developers to focus on the relevant problems for their code changes. 

#### Defining a Merge Request Policy
You can define a merge request policy for your integration branch that will block merge requests due to new violations. To configure this:
1. In your GitLab project view, go to Security & Compliance>Policies, and select New policy.
2. Select the Scan result policy type.
3. In the Policy details section, define a rule for Static Application Security Testing (select “IF SAST…”). Configure additional options, if needed.


## Unit Tests

### <a name="quick-start-UT"></a> Quick start

To collect test executions results for your code with Parasoft Jtest and review test results in GitLab, you need to customize your pipeline to include a job that will:
* build the tested project with Gradle or Maven.
* run Jtest.
* use Saxon to convert Jtest unit tests report to xUnit format.
* upload the transformed xUnit report.

#### Prerequisites

* This extension requires Parasoft Jtest 2020.2 (or newer) with a valid Parasoft license.
* We recommend that you execute the pipeline on a GitLab runner with Parasoft Jtest installed and configured on the runner.
* To support xUnit format, you need following files:
  - Saxon-HE: copy the folder [here](saxon) or download from [Saxonica](https://www.saxonica.com/download/java.xml).
  - [XSLT file](xsl/xunit.xsl) for transforming from Parasoft unit tests report to xUnit report

### <a name="example-pipelines-UT"></a> Example Pipelines
The following examples show simple pipelines for Gradle and Maven projects. The examples assume that Jtest is run on a GitLab runner and the path to the `jtestcli` executable is available on `PATH`.

##### Run Jtest with Gradle project

See also the example [.gitlab-ci.yml](https://gitlab.com/parasoft/jtest-gitlab/-/blob/master/pipelines/jtest-gradle/.gitlab-ci.yml) file.


```yaml
# This is a basic pipeline to help you get started with Jtest integration to collect test executions results of a Gradle project.

stages:
  - test

# Builds the project with Gradle to run unit tests with Jtest.
UnitTests:
  stage: test
  script:
    # Launches Jtest.
    - gradle -i jtest-agent test jtest -I path/to/jtest/integration/gradle/init.gradle "-Djtest.config=builtin://Unit Tests" "-Djtest.report=reports"

    # Convert Jtest unit tests report to xUnit format.
    # When running on Windows, be sure to replace backslashes:
      # - $CI_PROJECT_DIR = $CI_PROJECT_DIR.Replace("\", "/")
    - path/to/jtest/bin/jre/bin/java -jar 'saxon/saxon-he-12.2.jar' -xsl:"xsl/xunit.xsl" -s:"reports/report.xml" -o:"reports/report-xunit.xml" -t pipelineBuildWorkingDirectory=$CI_PROJECT_DIR
    # Notes: To use Saxon for transformation, a Java executable is required. Jtest is bundled with Java which can be used for this purpose.

  artifacts:
    reports:
      junit: reports/report-xunit.xml
```

##### Run Jtest with Maven project

See also the example [.gitlab-ci.yml](https://gitlab.com/parasoft/jtest-gitlab/-/blob/master/pipelines/jtest-maven/.gitlab-ci.yml) file.


```yaml
# This is a basic pipeline to help you get started with Jtest integration to collect test executions results of a Maven project.

stages:
  - test

# Builds the project with Maven to run unit tests with Jtest.
UnitTests:
  stage: test
  script:
    # Launches Jtest.
    - mvn process-test-classes jtest:agent test jtest:jtest "-Djtest.config=builtin://Unit Tests" "-Djtest.report=reports"

    # Convert Jtest unit tests report to xUnit format.
    # When running on Windows, be sure to replace backslashes:
      # - $CI_PROJECT_DIR = $CI_PROJECT_DIR.Replace("\", "/")
    - path/to/jtest/bin/jre/bin/java -jar 'saxon/saxon-he-12.2.jar' -xsl:"xsl/xunit.xsl" -s:"reports/report.xml" -o:"reports/report-xunit.xml" -t pipelineBuildWorkingDirectory=$CI_PROJECT_DIR
    # Notes: To use Saxon for transformation, a Java executable is required. Jtest is bundled with Java which can be used for this purpose.

  artifacts:
    reports:
      junit: reports/report-xunit.xml
```

### Reviewing Test Results
When the pipeline completes, you can review the test results handled by Jtest in the *Tests* tab of the GitLab pipeline.

## Code Coverage

### <a name="quick-start-CC"></a> Quick start

To collect code coverage data for your code with Parasoft Jtest and review coverage data in GitLab, you need to customize your pipeline to include a job that will:
* build the tested project with Gradle or Maven.
* run Jtest.
* use Saxon to convert Jtest coverage report to Cobertura format.
* upload the transformed Cobertura coverage report.

#### Prerequisites

* This extension requires Parasoft Jtest 2020.2 (or newer) with a valid Parasoft license.
* We recommend that you execute the pipeline on a GitLab runner with Parasoft Jtest installed and configured on the runner.
* To support Cobertura format, you need following files:
    - Saxon-HE: copy the folder [here](saxon) or download from [Saxonica](https://www.saxonica.com/download/java.xml).
    - [XSLT file](xsl/cobertura.xsl) for transforming from Parasoft coverage report to Cobertura report

### <a name="example-pipelines-CC"></a> Example Pipelines
The following examples show simple pipelines for Gradle and Maven projects. The examples assume that Jtest is run on a GitLab runner and the path to the `jtestcli` executable is available on `PATH`.

##### Run Jtest with Gradle project

See also the example [.gitlab-ci.yml](https://gitlab.com/parasoft/jtest-gitlab/-/blob/master/pipelines/jtest-gradle/.gitlab-ci.yml) file.


```yaml
# This is a basic pipeline to help you get started with Jtest integration to collect coverage of a Gradle project.

stages:
  - test

# Builds the project with Gradle to run code coverage with Jtest.
CodeCoverage:
  stage: test
  script:
    # Launches Jtest.
    - gradle -i jtest-agent test jtest -I path/to/jtest/integration/gradle/init.gradle "-Djtest.config=builtin://Unit Tests" "-Djtest.report=reports"

    # Convert Jtest coverage report to Cobertura format.
    # When running on Windows, be sure to replace backslashes:
      # - $CI_PROJECT_DIR = $CI_PROJECT_DIR.Replace("\", "/")
    - path/to/jtest/bin/jre/bin/java -jar 'saxon/saxon-he-12.2.jar' -xsl:"xsl/cobertura.xsl" -s:"reports/coverage.xml" -o:"reports/cobertura.xml" -t pipelineBuildWorkingDirectory=$CI_PROJECT_DIR
    # Notes: To use Saxon for transformation, a Java executable is required. Jtest is bundled with Java which can be used for this purpose.

  # Uploads code coverage data in the GitLab Cobertura format, so that they are displayed in GitLab.
  artifacts:
    reports:
      coverage_report:
        # Coverage report format.
        coverage_format: cobertura
        # Uploads Cobertura report file to enable test coverage visualization in Gitlab merge request.
        path: reports/cobertura.xml
```

##### Run Jtest with Maven project

See also the example [.gitlab-ci.yml](https://gitlab.com/parasoft/jtest-gitlab/-/blob/master/pipelines/jtest-maven/.gitlab-ci.yml) file.


```yaml
# This is a basic pipeline to help you get started with Jtest integration to collect coverage of a Maven project.

stages:
  - CodeCoverage

# Builds the project with Maven to run code coverage with Jtest.
CodeCoverage:
  stage: test
  script:
    # Launches Jtest.
    - mvn process-test-classes jtest:agent test jtest:jtest "-Djtest.config=builtin://Unit Tests" "-Djtest.report=reports"

    # Convert Jtest coverage report to Cobertura format.
    # When running on Windows, be sure to replace backslashes:
      # - $CI_PROJECT_DIR = $CI_PROJECT_DIR.Replace("\", "/")
    - path/to/jtest/bin/jre/bin/java -jar 'saxon/saxon-he-12.2.jar' -xsl:"xsl/cobertura.xsl" -s:"reports/coverage.xml" -o:"reports/cobertura.xml" -t pipelineBuildWorkingDirectory=$CI_PROJECT_DIR
    # Notes: To use Saxon for transformation, a Java executable is required. Jtest is bundled with Java which can be used for this purpose.

    # Uploads code coverage data in the GitLab Cobertura format, so that they are displayed in GitLab.
  artifacts:
    reports:
      coverage_report:
        # Coverage report format.
        coverage_format: cobertura
        # Uploads Cobertura report file to enable test coverage visualization in Gitlab merge request.
        path: reports/cobertura.xml
```

### Reviewing Coverage Data
After the pipeline triggered by a merge request completes, you can review the code coverage data collected by Jtest:
 * in the file diff view of the GitLab Merge requests.
 
Please visit the official GitLab website for more information about [Test coverage visualization](https://docs.gitlab.com/ee/ci/testing/test_coverage_visualization.html).
  


---
## About
Jtest integration for GitLab - Copyright (C) 2023 Parasoft Corporation
